angular
  .module('Company')
  .controller('employeesController', employeesController);

function employeesController($scope, dataFactory){

  $scope.data = {};             // All the data (comes from API)
  $scope.current = {};          // Data item to show in modal
  $scope.departments = {};

  // get data for view from API
  var loadData = function() {
    dataFactory.getEmployees().then(function(response) {
      console.log('got data from API:');
      $scope.data = response;
      console.log($scope.data);
    });
  };
  loadData();


  // Called from table "Näytä" button
  $scope.viewEmployee = function(item) {
    console.log("view modal for data item: " + item);
    $scope.current = item;
    $("#infoModal").modal(); // show the modal
  };


  $scope.createEmployee = function() {
    $scope.current = {};
    dataFactory.getDepartments().then(function(response) {
      $scope.departments = response.data;
      console.log($scope.departments);
      $("#createModal").modal(); // show the modal
    });
  };

  $scope.saveNew = function() {
    console.log($scope.current);
    dataFactory.createEmployee($scope.current)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };


  $scope.selectEmployee = function(employee) {
    console.log("selected employee " + employee.fname + " " + employee.lname);
    $scope.$parent.nameInNaviBar = employee.fname + " " + employee.lname;
  };

  $scope.deleteEmployee = function(employee) {
    dataFactory.deleteEmployee(employee)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };
}
