angular
  .module('Company')
  .controller('departmentsController', departmentsController);

function departmentsController($scope, dataFactory){

  $scope.data = {};             // All the data (comes from API)
  $scope.current = {};        // Data item to show in modal

  $scope.$parent.nameInNaviBar = "";

  // get data for view from API
  function loadData() {
    dataFactory.getDepartments().then(function(response) {
      console.log('got data from API:');
      $scope.data = response.data;
      console.log($scope.data);
    });
  }
  loadData();


  $scope.createNew = function() {
    $scope.current = {};
    $("#createModal").modal();
  };

  $scope.saveNew = function() {
    console.log($scope.current);
    dataFactory.createDepartment($scope.current)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };


  // Called from table "Näytä" button
  $scope.viewItem = function(item) {
    console.log("show modal for data item: " + item);
    $scope.current = item;
    $("#infoModal").modal(); // show the modal
  };

  $scope.deleteDepartment = function(dep) {
    dataFactory.deleteDepartment(dep)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };

}