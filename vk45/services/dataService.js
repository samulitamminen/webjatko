angular
  .module('Company')
  .factory("dataFactory", dataFactory);

function dataFactory($http, $q) {

  var factoryObject = {};

  var baseUrl = "http://home.tamk.fi/~kujesa/webjatko/rest/index.php";

  var get = function(path) {
    var url = baseUrl + path;
    var deferred = $q.defer(); // prepare for a promise
    $http.get(url).success (function(response) {
      deferred.resolve(response); // handle promised data
    });
    return deferred.promise; // return promise object
  };

  var post = function(path, data) {
    var url = baseUrl + path;
    var deferred = $q.defer();

    $http({
      method: 'POST',
      url: url,
      data: data,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .success (function(response) {
      deferred.resolve(response);
    });
    return deferred.promise;
  };


  // employees
  factoryObject.getEmployees = function() {
    return get("/employees");
  };

  factoryObject.getEmployeeById = function (obj) {
    return get("/employees" + obj.id);
  };

  factoryObject.updateEmployee = function (obj) {
    return post("/updateEmployee", obj);
  };

  factoryObject.deleteEmployee = function (obj) {
    return post("/deleteEmployee", obj);
  };

  factoryObject.createEmployee = function (obj) {
    return post("/createEmployee", obj);
  };



  // departments
  factoryObject.getDepartments = function () {
    return get("/departments");
  };

  factoryObject.deleteDepartment = function (obj) {
    return post("/deleteDepartment", obj);
  };

  factoryObject.createDepartment = function (obj) {
    return post("/createDepartment", obj);
  };


  // projects
  factoryObject.getProjects = function () {
    return get("/projects");
  };

  factoryObject.deleteProject = function (obj) {
    return post("/deleteProject", obj);
  };

  factoryObject.createProject = function (obj) {
    return post("/createProject", obj);
  };



  return factoryObject;    // return factory object
}
