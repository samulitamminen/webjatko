angular
  .module('Company')
  .controller('projectsController', projectsController);

function projectsController($scope, dataFactory){

  $scope.data = {};             // All the data (comes from API)
  $scope.current = {};          // Data item to show in modal
  $scope.employees = {};

  $scope.$parent.nameInNaviBar = "";

  // get data for view from API
  function loadData() {
    dataFactory.getProjects().then(function(response) {
      console.log('got data from API:');
      $scope.data = response.data;
      console.log($scope.data);
    });
  }
  loadData();

  // Called from table "Näytä" button
  $scope.viewItem = function(item) {
    console.log("show modal for data item: " + item);
    $scope.current = item;
    $("#infoModal").modal(); // show the modal
  };

  $scope.deleteProject = function(proj) {
    dataFactory.deleteProject(proj)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };

  $scope.createNew = function() {
    $scope.current = {};
    dataFactory.getEmployees().then(function(response) {
      $scope.employees = response;
      console.log($scope.employees);
      $("#createModal").modal(); // show the modal
    });
  };

  $scope.saveNew = function() {
    console.log($scope.current);
    dataFactory.createProject($scope.current)
    .then(function(response) {
      console.log(response);
      loadData();
    });
  };

}