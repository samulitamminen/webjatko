angular
  .module('Company')
  .config(companyConfig);

// configure our routes
function companyConfig($routeProvider) {
  $routeProvider

    .when('/', {
      templateUrl : 'employees/employees.html',
      controller  : 'employeesController'
    })
    .when('/employees', {
      templateUrl : 'employees/employees.html',
      controller  : 'employeesController'
    })
    .when('/departments', {
      templateUrl : 'departments/departments.html',
      controller  : 'departmentsController'
    })
    .when('/projects', {
      templateUrl : 'projects/projects.html',
      controller  : 'projectsController'
    });

}