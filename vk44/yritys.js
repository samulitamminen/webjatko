var myApp = angular.module("myApp", ['ngRoute']);

// configure our routes
myApp.config(function($routeProvider) {
  $routeProvider

    .when('/', {
      templateUrl : 'pages/tyontekijat.html',
      controller  : 'employeesController'
    })
    .when('/tyontekijat', {
      templateUrl : 'pages/tyontekijat.html',
      controller  : 'employeesController'
    })
    .when('/osastot', {
      templateUrl : 'pages/osastot.html',
      controller  : 'departmentsController'
    })
    .when('/projektit', {
      templateUrl : 'pages/projektit.html',
      controller  : 'projectsController'
    });

});


myApp.controller("mainController", function($scope, $http){
  $scope.nameInNaviBar = "";
});

myApp.controller("employeesController", function($scope, $http){

  $scope.data = {};             // All the data (comes from API)
  $scope.modalItem = {};        // Data item to show in modal

  // get data for view from API
  $http.get("http://home.tamk.fi/~kujesa/webjatko/rest/index.php/employees")
    .then(function(response) {
      console.log('got data from API:');
      $scope.data = response.data;
      console.log($scope.data);
    });

  // Called from table "Näytä" button
  $scope.showModal = function(item) {
    console.log("show modal for data item: " + item);
    $scope.modalItem = item;
    $("#infoModal").modal(); // show the modal
  };

  $scope.selectEmployee = function(employee) {
    console.log("selected employee " + employee.fname + " " + employee.lname);
    $scope.$parent.nameInNaviBar = employee.fname + " " + employee.lname;
  };

});


myApp.controller("departmentsController", function($scope, $http){

  $scope.data = {};             // All the data (comes from API)
  $scope.modalItem = {};        // Data item to show in modal

  $scope.$parent.nameInNaviBar = "";

  // get data for view from API
  $http.get("http://home.tamk.fi/~kujesa/webjatko/rest/index.php/departments")
    .then(function(response) {
      console.log('got data from API:');
      $scope.data = response.data.data; // inconsistency in API
      console.log($scope.data);
    });

  // Called from table "Näytä" button
  $scope.showModal = function(item) {
    console.log("show modal for data item: " + item);
    $scope.modalItem = item;
    $("#infoModal").modal(); // show the modal
  };

});


myApp.controller("projectsController", function($scope, $http){

  $scope.data = {};             // All the data (comes from API)
  $scope.modalItem = {};        // Data item to show in modal

  $scope.$parent.nameInNaviBar = "";

  // get data for view from API
  $http.get("http://home.tamk.fi/~kujesa/webjatko/rest/index.php/projects")
    .then(function(response) {
      console.log('got data from API:');
      $scope.data = response.data.data; // inconsistency in API
      console.log($scope.data);
    });

  // Called from table "Näytä" button
  $scope.showModal = function(item) {
    console.log("show modal for data item: " + item);
    $scope.modalItem = item;
    $("#infoModal").modal(); // show the modal
  };

});