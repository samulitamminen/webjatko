var myApp = angular.module("myApp", []);

myApp.controller("appController", function($scope, $location, $http){

  $scope.view = "employees";    // Current view (from url)
  $scope.data = {};             // All the data (comes from API)
  $scope.title = "Työntekijä";  // Page title
  $scope.titles = {};           // Titles for keys in data. Will be used in table listing
  $scope.modalItem = {};        // Data item to show in modal


  // Watch for url changes
  $scope.$watch(function () {
    return $location.path();
  }, function (view) {
    // if empty, set to employees
    view = view || 'employees';

    $scope.view = view.replace('/', '');
    console.log('view is now: ' + $scope.view);

    // gets new data from API
    updateDataForView($scope.view);

  });


  // Watches view and updates titles according to it
  $scope.$watch('view', function(view) {
    if (view === 'employees') {
      $scope.title = "Työntekijä";
      $scope.titles = {
        id:     "Id",
        fname:  "Etunimi",
        lname:  "Sukunimi",
        bdate:  "Syntymäaika",
        // dep:    "Dep?",
        // email:  "Sähköposti",
        // dname:  "Osasto",
        // image:  "Kuva",
        salary: "Palkka",
        phone1: "Puhelin",
        // phone2: "Puhelin 2",
      };
    }
    else if (view === 'departments') {
      $scope.title = "Osasto";
      $scope.titles = {
        id: "Id",
        name: "Nimi"
      };
    }
    else if (view === 'projects') {
      $scope.title = "Projekti";
      $scope.titles = {
        id: "Id",
        fname: "Etunimi",
        lname: "Sukunimi",
        // image: "Kuva",
        managerid: "Manager Id",
        name: "Nimi"
      };
    }
  });


  // Called from table "Näytä" button
  $scope.showModal = function(id) {
    console.log("show modal for id: " + id);
    $scope.modalItem = getItemById(id);
    $("#infoModal").modal(); // show the modal
  };


  // Used in ng-repeat for original order
  $scope.objectKeys = function(obj){
    return Object.keys(obj);
  };


  // Called when url changes
  var updateDataForView = function(view) {

    // get data for view from API
    var apiUrl = "http://home.tamk.fi/~kujesa/webjatko/rest/index.php/" + view;

    $http.get(apiUrl).then(function successCallback(response) {
      // this callback will be called asynchronously
      // when the response is available
      console.log('got data from API:');
      if (view == 'employees') {
        $scope.data = response.data;
      } else {
        $scope.data = response.data.data; // inconsistency in API json
      }
      console.log($scope.data);

    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log('error with API!');
      console.log(response);
    });
  
  };


  // Get item from data by the key 'id'
  var getItemById = function(id) {
    for (var i = 0; i < $scope.data.length; i++) {
      if ($scope.data[i].id === id) {
        return $scope.data[i];
      }
    }
  };

});