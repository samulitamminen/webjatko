var myApp = angular.module("myApp", []);

myApp.controller("appController", function($scope){

  $scope.names = [];

  $scope.add = function(name) {
    console.log("add " + name);
    $scope.names.push(name);
  };

  $scope.delete = function(index) {
    console.log("delete index " + index);
    $scope.names.splice(index, 1);
  };

});