var myApp = angular.module("myApp", []);

myApp.controller("appController", function($scope){
  $scope.countries = [
    {"ID":"1067","name":"China","population":"1359821466","percentage":"19,66%","position":"1"},
    {"ID":"1068","name":"India","population":"1205624727","percentage":"17,43%","position":"2"},
    {"ID":"1069","name":"United States of America","population":"312237216","percentage":"4,51%","position":"3"},
    {"ID":"1070","name":"Indonesia","population":"240676485","percentage":"3,48%","position":"4"},
    {"ID":"1071","name":"Brazil","population":"195210154","percentage":"2,82%","position":"5"},
    {"ID":"1072","name":"Pakistan","population":"173113821","percentage":"2,50%","position":"6"},
    {"ID":"1073","name":"Nigeria","population":"159685249","percentage":"2,31%","position":"7"},
    {"ID":"1074","name":"Bangladesh","population":"151125475","percentage":"2,19%","position":"8"},
    {"ID":"1075","name":"Russian Federation","population":"143615916","percentage":"2,08%","position":"9"},
    {"ID":"1076","name":"Japan","population":"127352833","percentage":"1,84%","position":"10"}
  ];
});