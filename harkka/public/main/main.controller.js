angular
  .module('Shopper')
  .controller('mainController', mainController);

function mainController($scope, $routeParams, dataFactory, $localStorage){
  
  $scope.products = [];
  $scope.categories = [];

  $scope.$storage = $localStorage;

  if ($scope.$storage.user === undefined) {
    $scope.$storage.user = "Customer";
  }

  $scope.$watch('products', function(newVal, oldVal) {
    if (newVal != oldVal) {
      console.log("Main: Broadcasted products.");
      $scope.$broadcast('productsUpdated', newVal);
    }
  });
  $scope.$watch('categories', function(newVal, oldVal) {
    if (newVal != oldVal) {
      console.log("Main: Broadcasted categories.");
      $scope.$broadcast('categoriesUpdated', newVal);
    }
  });

  // Get Data from service
  var loadData = function() {
    dataFactory.products().then(function(response) {
      console.log('Main: got products from Service:');
      console.log(response);
      $scope.products = response;
      // console.log($scope.products);
    });

    dataFactory.categories().then(function(response) {
      console.log('Main: got categories from Service:');
      console.log(response);
      $scope.categories = response;
      // console.log($scope.categories);
    });
  };
  loadData();

  $scope.toggleUser = function() {
    if ($scope.$storage.user === "Admin") {
      $scope.$storage.user = "Customer";
    } else {
      $scope.$storage.user = "Admin";
    }
  };

  // Refresh data from API
  // Can be called form child controllers
  $scope.refreshProducts = function() {
    dataFactory.refreshProducts().then(function(response) {
      dataFactory.products().then(function(response) {
        console.log('Main: Products refreshed.');
        // console.log(response);
        $scope.products = response;
      });
    });
  };
  $scope.refreshCategories = function() {
    dataFactory.refreshCategories().then(function(response) {
      dataFactory.categories().then(function(response) {
        console.log('Main: Categories refreshed.');
        // console.log(response);
        $scope.categories = response;
      });
    });
  };


}
