angular
  .module('Shopper')
  .config(config);

// configure our routes
function config($routeProvider) {
  $routeProvider

    // .when('/', {
    //   templateUrl : 'main/main.html',
    //   controller  : 'mainController'
    // })

    .when('/categories/:category', {
      templateUrl : 'categories/categories.html',
      controller  : 'categoriesController'
    })

    .when('/products/:product', {
      templateUrl : 'products/products.html',
      controller  : 'productsController'
    })

    .when('/admin', {
      redirectTo: '/admin/products'
    })
    .when('/admin/products', {
      templateUrl : 'admin/admin.products.html',
      controller  : 'adminProductsController'
    })
    .when('/admin/categories', {
      templateUrl : 'admin/admin.categories.html',
      controller  : 'adminCategoriesController'
    })
    .when('/admin/orders', {
      templateUrl : 'admin/admin.orders.html',
      controller  : 'adminOrdersController'
    })

    .when('/checkout', {
      templateUrl : 'checkout/checkout.html',
      controller  : 'checkoutController'
    })

    .otherwise({
      redirectTo: '/categories/all'
    });

}