angular
  .module('Shopper')
  .controller('productsController', productsController);

function productsController($scope, $routeParams, dataFactory, cartFactory, $localStorage){
  
  $scope.products = [];
  $scope.categories = [];

  $scope.category = "";
  $scope.product = {};

  $scope.cart = cartFactory.getCart();

  var loadData = function() {
    dataFactory.products().then(function(response) {
      console.log('got products from service:');
      console.log(response);
      $scope.products = response;
      // console.log($scope.products);
      
      // Get the Product from url (or the first)
      var productId = $routeParams.product;
      var productIndex = indexOfProduct(productId);
      if (productId !== undefined && productIndex >= 0) {
        $scope.product = $scope.products[productIndex];
      } else {
        $scope.product = $scope.products[0];
      }
      $scope.category = $scope.product.category;

    });
    dataFactory.categories().then(function(response) {
      console.log('got categories from service:');
      console.log(response);
      $scope.categories = response;
      // console.log($scope.categories);
    });
  };
  loadData();



  



  $scope.addToCart = function(product) {
    cartFactory.addToCart(product);
  };

  $scope.removeFromCart = function(index) {
    cartFactory.removeFromCart(index);
  };

  function indexOfProduct(id) {
    for(var i = 0; i < $scope.products.length; i++) {
      if ($scope.products[i].id === id) {
        return i;
      }
    }
    return -1;
  }

}
