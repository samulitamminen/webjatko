angular
  .module('Shopper')
  .factory('cartFactory', cartFactory);

function cartFactory($localStorage) {

  var factoryObject = {};

  factoryObject.getCart = function() {
    if ($localStorage.cart === undefined) {
      console.log("localstorage cart is not yet defined");
      $localStorage.cart = {"sum": 0, "products": []};
    }
    return $localStorage.cart;
  };

  factoryObject.addToCart = function(product) {

    var index = isInCart(product);

    if (index >= 0) {
      $localStorage.cart.products[index].count++;
    } else {
      var cartItem = {
        "count": 1,
        "product": product
      };
      $localStorage.cart.products.push(cartItem);
    }

    $localStorage.cart.sum = Number($localStorage.cart.sum) + Number(product.price);

  };

  factoryObject.removeFromCart = function(index) {

    $localStorage.cart.sum = Number($localStorage.cart.sum) - Number($localStorage.cart.products[index].product.price);

    if ($localStorage.cart.products[index].count > 1) {
      $localStorage.cart.products[index].count--;
    } else {
      $localStorage.cart.products.splice(index, 1);
    }

  };

  factoryObject.clearCart = function() {
    $localStorage.cart.sum = 0;
    $localStorage.cart.products = [];
  };


  /**
   * return index of product in cart
   * or -1 if not found
   */
  function isInCart(product) {
    if ($localStorage.cart === undefined || $localStorage.cart.products.length === 0) {
      return -1;
    }
    for (var i = 0; i < $localStorage.cart.products.length; i++) {
      if ($localStorage.cart.products[i].product.id === product.id) {
        return i;
      }
    }
    return -1;
  }

  return factoryObject;
}