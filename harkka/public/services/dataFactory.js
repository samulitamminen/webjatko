angular
  .module('Shopper')
  .factory("dataFactory", dataFactory);

function dataFactory($http, $q) {

  var factoryObject = {};

  // Load items only once and then use from here
  var products = [];
  var categories = [];

  //var baseUrl = "http://192.168.33.10/rest/index.php";
  var baseUrl = "http://koti.tamk.fi/~e2stammi/webohj/harkka/server/public/rest/index.php";

  var get = function(path) {
    var url = baseUrl + path;
    var deferred = $q.defer(); // prepare for a promise
    $http.get(url).success (function(response) {
      deferred.resolve(response); // handle promised data
    });
    return deferred.promise; // return promise object
  };

  var post = function(path, data) {
    var url = baseUrl + path;
    var deferred = $q.defer();

    var postData = 'data=' + encodeURIComponent(JSON.stringify(data));

    console.log("POST data:");
    console.log(postData);

    $http({
      method: 'POST',
      url: url,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .success (function(response) {
      deferred.resolve(response);
    });
    return deferred.promise;
  };



  factoryObject.products = function() {
    var deferred = $q.defer();

    if (products.length > 0) {
      console.log("Products already loaded.");
      deferred.resolve(products);
      return deferred.promise;
    } else {
      console.log("Products not yet loaded.");
      $http.get(baseUrl + "/getProducts").success (function(response) {
        products = response;
        deferred.resolve(response); // handle promised data
      });
      return deferred.promise;
    }
  };
  factoryObject.refreshProducts = function() {
    return get("/getProducts").then(function(response) {
      console.log('dataFactory: Products refreshed.');
      products = response;
    });
  };

  factoryObject.categories = function() {
    var deferred = $q.defer();

    if (categories.length > 0) {
      console.log("Categories already loaded.");
      deferred.resolve(categories);
      return deferred.promise;
    } else {
      console.log("Categories not yet loaded.");
      $http.get(baseUrl + "/getCategories").success (function(response) {
        categories = response;
        deferred.resolve(response); // handle promised data
      });
      return deferred.promise;
    }
  };
  factoryObject.refreshCategories = function() {
    return get("/getCategories").then(function(response) {
      console.log('dataFactory: Categories refreshed.');
      categories = response;
    });
  };

  // Products
  factoryObject.getProducts = function() {
    return get("/getProducts");
  };
  factoryObject.createProduct = function (obj) {
    return post("/createProduct", obj);
  };
  factoryObject.updateProduct = function (obj) {
    return post("/updateProduct", obj);
  };
  factoryObject.deleteProduct = function (obj) {
    return post("/deleteProduct", obj);
  };


  // Categories
  factoryObject.getCategories = function() {
    return get("/getCategories");
  };
  factoryObject.createCategory = function (obj) {
    return post("/createCategory", obj);
  };
  factoryObject.updateCategory = function (obj) {
    return post("/updateCategory", obj);
  };
  factoryObject.deleteCategory = function (obj) {
    return post("/deleteCategory", obj);
  };


  // Orders
  factoryObject.getOrders = function() {
    return get("/getOrders");
  };
  factoryObject.createOrder = function (obj) {
    return post("/createOrder", obj);
  };
  factoryObject.updateOrder = function (obj) {
    return post("/updateOrder", obj);
  };
  factoryObject.deleteOrder = function (obj) {
    return post("/deleteOrder", obj);
  };


  return factoryObject;    // return factory object
}
