angular
  .module('Shopper')
  .controller('checkoutController', checkoutController);

function checkoutController($scope, dataFactory, cartFactory, $location){

  $scope.cart = cartFactory.getCart();
  //console.log($scope.cart);
  
  $scope.order = {
    "id": Date.now(),
    "status": "Processing",

    "customer": {
      "name": "",
      "address": "",
      "postcode": "",
      "city": "",
      "email": "",
      "phone": ""
    },

    "shippingMethod": {
      "name": "",
      "price": ""
    },

    "paymentMethod": "",

    "cart": $scope.cart

  };

  // Main controller broadcasts these events
  $scope.$on('productsUpdated', function(event, products) {
    $scope.products = products;
  });
  $scope.$on('categoriesUpdated', function(event, categories) {
    $scope.categories = categories;
  });


  $scope.placeOrder = function() {
    if (missingFields()) {
      alert("All fields are required!");
      return;
    }

    console.log("checkout.placeOrder: ");
    console.log($scope.order);

    dataFactory.createOrder($scope.order)
    .then(function(response) {
      console.log("placeOrder.createOrder: ");
      console.log(response);
      cartFactory.clearCart();
      $("#thanksModal").modal();
    });
  };

  $scope.finishCheckout = function() {
    $('#thanksModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $location.path("#/categories/all");
  };

  function missingFields() {
    if ($scope.order.customer.name === "" ||
      $scope.order.customer.address === "" ||
      $scope.order.customer.postcode === "" ||
      $scope.order.customer.city === "" ||
      $scope.order.customer.email === "" ||
      $scope.order.customer.phone === "" ||
      $scope.order.shippingMethod.name === "" ||
      $scope.order.paymentMethod === "" ||
      $scope.order.cart.products === []) {
      return true;
    }
    return false;
  }

  $scope.shippingMethods = [
    {
      "name": "Pickup",
      "price": 0.00
    },
    {
      "name": "SmartPOST",
      "price": 4.99
    },
    {
      "name": "Matkahuolto",
      "price": 6.99
    },
    {
      "name": "UPS",
      "price": 9.99
    }
  ];

  $scope.paymentMethods = [
    "Cash On Delivery",
    "Credit Card",
    "Bitcoin"
  ];

}
