angular
  .module('Shopper')
  .controller('adminOrdersController', adminOrdersController);

function adminOrdersController($scope, $routeParams, dataFactory){

  $scope.page = "orders";
  $scope.orders = [];
  $scope.currentOrder = {};

  // Main controller broadcasts these events
  $scope.$on('productsUpdated', function(event, products) {
    $scope.products = products;
  });
  $scope.$on('categoriesUpdated', function(event, categories) {
    $scope.categories = categories;
  });

  // Orders needed only on this page, not necessary to get them in main controller
  function getOrders() {
    dataFactory.getOrders().then(function(response) {
      console.log('Admin.Orders: got Orders from API:');
      console.log(response);
      $scope.orders = response;
      // console.log($scope.products);
    });
  }
  getOrders();


  $scope.viewOrder = function(order) {
    $scope.currentOrder = order;
    $("#orderModal").modal();
  };

  $scope.shipCurrent = function() {
    $scope.currentOrder.status = "Shipped";
    dataFactory.updateOrder($scope.currentOrder)
    .then(function(response) {
      console.log(response);
    });
  };

  $scope.deleteCurrent = function() {
    dataFactory.deleteOrder($scope.currentOrder)
    .then(function(response) {
      console.log(response);
      getOrders();
    });
  };

}
