angular
  .module('Shopper')
  .controller('adminProductsController', adminProductsController);

function adminProductsController($scope, $routeParams, dataFactory){

  $scope.page = "products";
  $scope.currentProduct = {};

  // Main controller broadcasts these events
  $scope.$on('productsUpdated', function(event, products) {
    $scope.products = products;
  });
  $scope.$on('categoriesUpdated', function(event, categories) {
    $scope.categories = categories;
  });


  $scope.createProduct = function() {
    $scope.currentProduct = {};
    $scope.originalProduct = {"id": ""};
    $("#productModal").modal();
  };
  $scope.editProduct = function(product) {
    $scope.currentProduct = product;
    $scope.originalProduct = product;
    $("#productModal").modal();
  };
  $scope.deleteProduct = function(product) {
    dataFactory.deleteProduct(product)
    .then(function(response) {
      console.log(response);
      $scope.$parent.refreshProducts();
    });
  };
  $scope.saveProduct = function() {
    if ($scope.originalProduct.id === "") {
      // Create New Product
      console.log("Admin: Create New Product");
      dataFactory.createProduct($scope.currentProduct)
      .then(function(response) {
        //console.log(response);
        $scope.$parent.refreshProducts();
      });
    } else {
      // Update product
      
      // Is id changed?
      if ($scope.originalProduct.id !== $scope.currentProduct.id) {
        console.log("Admin: Delete original and Create new");
        // first delete the original product (old id)
        dataFactory.deleteProduct($scope.originalProduct)
        .then(function(response) {
          //console.log(response);
        });
        // then create new with new id
        dataFactory.createProduct($scope.currentProduct)
        .then(function(response) {
          //console.log(response);
          $scope.$parent.refreshProducts();
        });
      } else {
        console.log("Admin: Update Product");
        // id not changed, just update the product
        dataFactory.updateProduct($scope.currentProduct)
        .then(function(response) {
          //console.log(response);
          $scope.$parent.refreshProducts();
        });
      }
    }
  };

}
