angular
  .module('Shopper')
  .directive('categoryList', categoryList)
  .directive('shoppingCart', shoppingCart);


function categoryList() {
  return {
    templateUrl: 'directives/categoryList.template.html',
  };
}

function shoppingCart() {
  return {
    templateUrl: 'directives/shoppingCart.template.html',
  };
}