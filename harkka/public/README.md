# Shopper

## Data Models

### Product

```js
{
  "id": "unique identifier",
  "name": "Product Name",
  "description": "Long bla bla text",
  "price": "34.99",
  "category": "category slug",
  "image": "image url"
}
```

### Category

```js
{
  "id": "name-1",
  "name": "Name 1"
}
```

### Order

```js
{
  // ID increments from here
  "id": 1000000,

  // "Processing" or "Shipped"
  "status": "Processing"

  // Info from checkout page
  "customer": {
    "name": "",
    "address": "",
    "city": "",
    "postcode": "",
  },
  "shippingMethod": {
    "name": "SmartPOST",
    "price": 4.99
  },
  "paymentMethod": "Credit Card",

  // Cart object from store
  "cart": {
    "sum": 399,
    "products": [
      product1,
      product2
    ]
  }
}
```
