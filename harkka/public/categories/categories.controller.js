angular
  .module('Shopper')
  .controller('categoriesController', categoriesController);

function categoriesController($scope, $routeParams, dataFactory, cartFactory, $localStorage){
  
  $scope.cart = cartFactory.getCart();

  $scope.category = $routeParams.category;
  console.log("Category: " + $scope.category);

  // Main controller broadcasts these events
  $scope.$on('productsUpdated', function(event, products) {
    $scope.products = products;
  });
  $scope.$on('categoriesUpdated', function(event, categories) {
    $scope.categories = categories;
  });

  $scope.addToCart = function(product) {
    cartFactory.addToCart(product);
  };

  $scope.removeFromCart = function(index) {
    cartFactory.removeFromCart(index);
  };

  // Filter visible products by category
  $scope.categoryFilter = function(item) {
    if ($scope.category === "all") {
      return true;
    } else {
      return item.category === $scope.category;
    }
  };

}
