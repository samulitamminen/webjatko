<?php

/*

/index.php
  /getProducts - GET
  /createProduct - POST (parametrina json-string)
  /updateProduct - POST (parametrina json-string)
  /deleteProduct - POST (parametrina json-string)

  ja kategorioille samat

 */

header("Access-Control-Allow-Origin: *");


$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

// in config.php
// $db_host = "";
// $db_user = "";
// $db_pass = "";
// $db_database = "";
include "config.php";

$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_database);
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if (strlen($request[0]) == 0 ) {
  echo "url parameter missing";
  die;
}

switch ($request[0]) {
  case "getProducts":
    get("products");
    break;
  case "createProduct":
    create("products");
    break;
  case "updateProduct":
    update("products");
    break;
  case "deleteProduct":
    delete("products");
    break;

  case "getCategories":
    get("categories");
    break;
  case "createCategory":
    create("categories");
    break;
  case "updateCategory":
    update("categories");
    break;
  case "deleteCategory":
    delete("categories");
    break;

  case "getOrders":
    get("orders");
    break;
  case "createOrder":
    create("orders");
    break;
  case "updateOrder":
    update("orders");
    break;
  case "deleteOrder":
    delete("orders");
    break;

  default:
    break;
}

$mysqli->close();



function get($item) {
  if ($item == "products" || $item == "categories" || $item == "orders") {
    $tablename = $item;

    global $request, $mysqli;
    $sql = "SELECT data FROM $tablename";
    $result = $mysqli->query($sql);

    // $items = $result->fetch_all();
    $items = array();
    while($row = $result->fetch_assoc()) {
      $items[] = $row;
    }

    $count = sizeof($items);

    echo "[";
    for ($i=0; $i<$count; $i++) {
      // Print the json-string
      echo $items[$i]["data"];

      if ($i < $count - 1 ) {
        echo ",";
      }
    }
    echo "]";


  } else {
    echo "Unknown type.";
  }
}


function create($item) {
  if ($item == "products" || $item == "categories" || $item == "orders") {
    $tablename = $item;
    global $request, $mysqli;

    $data = $_POST["data"];
    $id = json_decode($data, true)["id"];
    var_dump($data);
    var_dump($id);

    if (strlen($data) > 0 && strlen($id) > 0) {

      if ($stmt = $mysqli->prepare("INSERT INTO $tablename (id, data) VALUES (?, ?)")) {
        $stmt->bind_param("ss", $id, $data);
        $stmt->execute();
        $stmt->close();
        echo $data;
      }

    } else {
      echo "Missing parameter data!";
    }
  } else {
    echo "Unknown type.";
  }
}

function update($item) {
  if ($item == "products" || $item == "categories" || $item == "orders") {
    $tablename = $item;
    global $request, $mysqli;

    $data = $_POST["data"];
    $id = json_decode($data, true)["id"];

    if (strlen($data) > 0 && strlen($id) > 0) {

      if ($stmt = $mysqli->prepare("UPDATE $tablename SET data=? WHERE id=?")) {
        $stmt->bind_param("ss", $data, $id);
        $stmt->execute();
        $stmt->close();
        echo $data;
      }

    } else {
      echo "missing parameter data or id";
    }
  } else {
    echo "Unknown type.";
  }
}

function delete($item) {
  if ($item == "products" || $item == "categories" || $item == "orders") {
    $tablename = $item;
    global $request, $mysqli;

    $data = $_POST["data"];
    $id = json_decode($data, true)["id"];

    if (strlen($data) > 0 && strlen($id) > 0) {

      if ($stmt = $mysqli->prepare("DELETE FROM $tablename WHERE id=?")) {
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $stmt->close();
        echo $data;
      }

    } else {
      echo "missing parameter data or id";
    }
  } else {
    echo "Unknown type.";
  }
}
