

// kutsutaan "Näytä"-napin onclick-eventillä
function openModalForEmployee(id) {

  // get the employee by id from API
  var employee;
  var apiurl = "http://home.tamk.fi/~kujesa/webjatko/rest/index.php/employees/";
  $.get(apiurl + id, function(data) {
    employee = JSON.parse(data);
    employee = employee.data;

    console.log(employee);

    var html = '' +
      '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<h4 class="modal-title">Työntekijän tiedot</h4>' +
      '</div>' +
      '<div class="modal-body">' +
          '<strong>Nimi:</strong> ' + employee.fname + ' ' + employee.lname + '<br>' +
          '<strong>Osasto:</strong> ' + employee.dname + '<br>' +
          '<strong>Sähköposti:</strong> ' + employee.email + '<br>' +
          '<strong>Puhelin:</strong> ' + employee.phone1 + ' ' + ((employee.phone2 !== null) ? 'ja ' + employee.phone2 : '') + '<br>' +
          '<strong>Palkka:</strong> ' + employee.salary + '<br>' +
          '<strong>Syntymäaika:</strong> ' + employee.bdate + '<br>' +
      '</div>' +
      '<div class="modal-footer">' +
        '<button type="button" class="btn btn-primary">' +
          '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save' +
        '</button>' +
        '<button type="button" class="btn btn-warning" data-dismiss="modal">' +
          '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cancel' +
        '</button>' +
      '</div>';

    // add content to modal
    $("#employeeModal .modal-content").html(html);

    // show the modal
    $("#employeeModal").modal();

  });
}


// generoidaan työntekijälle taulukkorivi
function getHtmlStringForEmployee(employee) {
  var html = '' +
  '<tr>' +
    '<td>' + employee.id + '</td>' +
    '<td>' + employee.fname + '</td>' +
    '<td>' + employee.lname + '</td>' +
    '<td>' + employee.bdate + '</td>' +
    '<td>' + employee.salary + '</td>' +
    '<td>' + employee.phone1 + '</td>' +
    '<td>' +
      '<button type="button" onclick="openModalForEmployee(' + employee.id + ')" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Näytä</button> ' +
      '<button type="button" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Muokkaa</button> ' +
      '<button type="button" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Poista</button>' +
    '</td>' +
  '</tr>';

  return html;
}

function getEmployees() {
  var url = "http://home.tamk.fi/~kujesa/webjatko/rest/index.php/employees";
  $.get(url, function(data) {
    var employees = JSON.parse(data);
    var htmlstring = "";
    for (var i = 0; i<employees.length; i++) {
      // generoidaan html riville
      htmlstring += getHtmlStringForEmployee(employees[i]);
    }
    $("#employees").append(htmlstring); // lisätään rivi taulukkoon
  });
}

// shorthand for $( document ).ready(function() {
$(function() {
  getEmployees();
});