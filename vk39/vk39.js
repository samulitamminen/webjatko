
/**

Tehtävä 1
Tee Javascriptia käyttävä sivu.

Näytä sivulla tämän hetkiset: Päiväys, viikonpäivä ja aika
Käytä tiedoissa suomalaista yleistä standardia (US-formaatti ei kelpaa)
Esimerkki: Maanantai 31.8.2015 klo 9:40
Toteuta sivu niin, että HTML ja Javascript ovat eri tiedostoissa.

Tehtävä 2
Toteuta juoksevan kellon näytäminen. Voit tehdä toiminnan JS1 päälle tai voit tehdä uuden version.

Näytä päiväys ja tämän hetkinen jatkuvasti päivittyvä aika sekunnin tarkkudella: Maanantai 31.8.2015 klo 9:40
Vinkki: Voit käyttää esim. window.setInterval -funktiota

 */

var weekdays = [
  "Maanantai",
  "Tiistai",
  "Keskiviikko",
  "Torstai",
  "Perjantai",
  "Lauantai",
  "Sunnuntai"
];

var updateDate = function() {
  var date = new Date();

  document.getElementById("day").innerHTML   =  weekdays[date.getDay()];
  document.getElementById("date").innerHTML  =  date.getDate() + "." + date.getMonth() + "." + date.getFullYear();
  document.getElementById("clock").innerHTML =  "klo " +
                                                ('0' + date.getHours()).slice(-2) + ":" +
                                                ('0' + date.getMinutes()).slice(-2) + ":" +
                                                ('0' + date.getSeconds()).slice(-2);
                                                // .slice(-2) palauttaa kaksi viimeistä merkkiä
                                                // 0 + 9  => 09
                                                // 0 + 10 => 10
  // kutsuu rekursiivisesti itseään
  setTimeout(updateDate, 500);
};

// ajetaan kerran, funktio kutsuu itseään loputtomasti
updateDate();


/*

Tehtävä 3
Toteuta vuorovaikutteinen sivu.

Tee sivun toiminta alla olevien kuvien mukaiseksi
Voit toteuttaa sivun suomeksi tai englanniksi

 */

// klikkilaskuri
var counter = 0;

// funktio, jota kutsutaan klikatessa elementtiä
var updateOddEven = function() {

  var content = "" + counter;
  content += (counter % 2 === 0) ? " even" : " odd";

  document.getElementById("oddeven").innerHTML = content;

  counter++;

};

// lisätään "kuuntelija" (kun klikataan elementtiä, suoritetaan funktio)
document.getElementById("oddeven").addEventListener("click", updateOddEven);
